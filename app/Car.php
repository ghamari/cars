<?php

namespace App;

use App\Util\Filters\Filterable;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{

   use  Filterable;

    protected $tablename="cars";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable  =['model_id','model_make_id', 'model_name',
        'model_trim','model_year','model_body',
        'model_engine_position','model_engine_type','model_engine_compression','model_engine_fuel',
        'make_country','model_weight_kg','model_transmission_type','image','price'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'tags'
    ];

    /**
     * Get the key name for route model binding.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'id';
    }

    /**
     * Get  image full pathName alias as photo
     *
     * @return string filename
     */
    public function getPhotoAttribute()
    {
        if($this->image){
            return url('/uploads/'.$this->image);
        }
        return url('/uploads/default.png');
    }

    /**
     * Get image Thumb fullPathName
     *
     * @return string filename
     */
    public function getThumbAttribute()
    {
        if($this->image){
            return url('/uploads/thumb-'.$this->image);
        }
        return url('/uploads/default.png');
    }


    /**
     * Get all the tags that belong to the car.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

}
