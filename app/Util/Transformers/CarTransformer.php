<?php

namespace App\Util\Transformers;

class CarTransformer extends Transformer
{
    protected $resourceName = 'car';


    /**
     * Transform a collection of Car items we can add extra data like custom attribute definded in Car model.
     *
     * @param Collection $car
     * @return array
     */
    public function transform($car)
    {
        $car['photo']=$car->photo;
        $car['thumb']=$car->thumb;
        return $car;

    }
}