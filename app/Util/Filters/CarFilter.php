<?php

namespace App\Util\Filters;



class CarFilter extends Filter
{
    /**
     * Filter by modelName.
     * Get all the cars by the given modelName.
     *
     * @param $modelName
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function model_name($model_name)
    {
        return $this->builder->where('model_name','like',$model_name.'%');
    }

    /**
     * Filter by min_price.
     * Get all the cars have minimum price .
     *
     * @param $min_price
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function min_price($min_price)
    {
        return $this->builder->where('price','>=',$min_price)->orwhereNull('price');
    }

    /**
     * Filter by max_price.
     * Get all the cars have maximum price .
     *
     * @param $max_price
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function max_price($max_price)
    {
        return $this->builder->where('price','<=',$max_price)->orwhereNull('price');
    }

    /**
     * Filter by make_country name.
     * Get all the cars  by the given make_country name.
     *
     * @param $make_country
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function make_country($make_country)
    {
        return $this->builder->where('make_country','like',$make_country.'%');
    }
}