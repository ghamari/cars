<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class sync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:db';


    /**
     * The name if table.
     *
     * @var string
     */
    protected $table = 'cars';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'sync data with remote api carqueryapi.com';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $this->info('syncing data from remote api...');
            $api=env('api_carquery','https://www.carqueryapi.com/api/0.3/?cmd=getTrims');
            $api_result=file_get_contents($api);
            $data = json_decode($api_result, true);
            $this->insertOrUpdate($data['Trims']);
            $this->info('Data Synced :)');
        }
        catch(\Exception $e){
            $this->error('Something went wrong!');
            throw new \Exception($e, 201);
        }
    }



    /**
     * Mass (bulk) insert or update on duplicate
     *
     * insertOrUpdate([
     *   ['id'=>1,'value'=>10],
     *   ['id'=>2,'value'=>60]
     * ]);
     *
     *
     * @param array $rows
     */
    function insertOrUpdate(array $rows){
        $table = $this->table;

        $first = reset($rows);

        $columns = implode( ',',
            array_map( function( $value ) { return "$value"; } , array_keys($first) )
        );


        $values = implode( ',', array_map( function( $row ) {
                return '('.implode( ',',
                        array_map( function( $value ) { return '"'.str_replace('"', '""', $value).'"'; } , $row )
                    ).')';
            } , $rows )
        );

        $values=(str_replace('""','null',$values));

        $updates = implode( ',',
            array_map( function( $value ) { return "$value = VALUES($value)"; } , array_keys($first) )
        );


        $sql = "INSERT INTO {$table}({$columns}) VALUES {$values} ON DUPLICATE KEY UPDATE {$updates}";

        return \DB::statement( $sql );
    }
}
