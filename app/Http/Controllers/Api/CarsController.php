<?php

namespace App\Http\Controllers\Api;

use App\Car;
use App\Util\Paginate\Paginate;
use App\Util\Filters\CarFilter;
use App\Http\Requests\Api\CreateCar;
use App\Http\Requests\Api\UpdateCar;
use App\Util\Transformers\CarTransformer;
use Illuminate\Http\Request;
use Image;
use Illuminate\Http\UploadedFile;

class CarsController extends ApiController
{
    /**
     * CarController constructor.
     *
     * @param CarTransformer $transformer
     */
    public function __construct(CarTransformer $transformer)
    {
        $this->transformer = $transformer;

    }

    /**
     * Get all the cars.
     *
     * @param CarFilter $filter
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(CarFilter $filter)
    {

        $cars = new Paginate(Car::filter($filter));

        return $this->respondWithPagination($cars);
    }


    /**
     * Create a new car and return the car if successful.
     *
     * @param CreateCar $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateCar $request)
    {
        $inputs=$request->all();

        if(empty($request->price)){
            $inputs= array_merge($inputs, [
                'price' => rand(4000,12000) *10,
            ]);
        }

        // upload image and make thumbnail
        if($request->has('image')){
            $inputs['image']=$this->moveUploadedFile($request->file('image'));
        }

        $car = Car::create($inputs);

        return $this->respondWithTransformer($car);
    }

    /**
     * Get the car given by its id.
     *
     * @param Car $car
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Car $car)
    {
        return $this->respondWithTransformer($car);
    }

    /**
     * Update the car given by its id and return the car if successful.
     *
     * @param UpdateCar $request
     * @param Car $car
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateCar $request, Car $car)
    {
        $inputs=$request->all();

        if(empty($request->price)){
            $inputs= array_merge($inputs, [
                'price' => rand(4000,12000) *10,
            ]);
        }

        // upload image and make thumbnail
        if($request->has('image')){
            $inputs['image']=$this->moveUploadedFile($request->file('image'));
        }

        $car->update($inputs);

        return $this->respondWithTransformer($car);
    }

    /**
     * Delete the car given by its slug.
     *
     * @param DeleteCar $request
     * @param Car $car
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Car $car)
    {
        $car->delete();

        return $this->respondSuccess();
    }



    /**
     * Moves the uploaded file to the upload directory and assigns it a unique name
     * to avoid overwriting an existing uploaded file.
     *
     * @param UploadedFile $uploaded file uploaded file to move
     * @return string filename of moved file
     */
    function moveUploadedFile(UploadedFile $uploadedFile)
    {
        try{
            $destinationPath='uploads'; // storage_path().'/uploadfiles/';
            $extension = $uploadedFile->getClientOriginalExtension(); // getting file extension
            $fileName = md5(microtime()) .".". $extension;// renaming image to uniq name
            $fullFileName=$destinationPath."/".$fileName;

            $uploadedFile->move($destinationPath, $fullFileName);
            $thumb=Image::make($fullFileName)->resize(100, 100)->save($destinationPath."/thumb-".$fileName);

            return $fileName;
        }
        catch(\Exception $e){
            throw new \Exception($e, 201);
        }
    }
}
