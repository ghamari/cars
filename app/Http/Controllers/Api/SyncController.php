<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Artisan;

class SyncController extends ApiController
{

    public $table='cars';

    /**
     * sync all the cars by calling artisan command php artisan sync:db.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {

        try {

            Artisan::call('sync:db');
            return Artisan::output();
        }
        catch(\Exception $e){
            throw new \Exception($e, 201);
        }
    }


}
