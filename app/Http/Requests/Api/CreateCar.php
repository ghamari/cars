<?php

namespace App\Http\Requests\Api;

class CreateCar extends ApiRequest
{
    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
//    protected function validationData()
//    {
//        return $this->get('car') ?: [];
//    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'model_id' => 'required',
        ];
    }
}
