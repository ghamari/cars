<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('model_id');
            $table->decimal('price',10,2)->nullable();
            $table->string('image')->nullable();
            $table->string('model_make_id')->nullable();
            $table->string('model_name')->nullable();
            $table->string('model_trim')->nullable();
            $table->string('model_year')->nullable();
            $table->string('model_body')->nullable();
            $table->string('model_engine_position')->nullable();
            $table->integer('model_engine_cc')->nullable();
            $table->integer('model_engine_cyl')->nullable();
            $table->string('model_engine_type')->nullable();
            $table->string('model_engine_stroke_mm')->nullable();
            $table->integer('model_engine_valves_per_cyl')->nullable();
            $table->integer('model_engine_power_ps')->nullable();
            $table->string('model_engine_power_rpm')->nullable();
            $table->integer('model_engine_torque_nm')->nullable();
            $table->integer('model_engine_torque_rpm')->nullable();
            $table->string('model_engine_bore_mm')->nullable();
            $table->string('model_engine_compression',100)->nullable();
            $table->string('model_engine_fuel')->nullable();
            $table->integer('model_top_speed_kph')->nullable();
            $table->integer('model_0_to_100_kph')->nullable();
            $table->string('model_drive')->nullable();
            $table->string('model_transmission_type')->nullable();
            $table->string('model_seats')->nullable();
            $table->integer('model_doors')->nullable();
            $table->integer('model_weight_kg')->nullable();
            $table->integer('model_length_mm')->nullable();
            $table->integer('model_width_mm')->nullable();
            $table->integer('model_height_mm')->nullable();
            $table->integer('model_wheelbase_mm')->nullable();
            $table->decimal('model_lkm_hwy',10,2)->nullable();
            $table->decimal('model_lkm_mixed',10,2)->nullable();
            $table->decimal('model_lkm_city',10,2)->nullable();
            $table->decimal('model_fuel_cap_l',10,2)->nullable();
            $table->integer('model_sold_in_us')->nullable();
            $table->string('model_co2')->nullable();
            $table->string('model_make_display')->nullable();
            $table->string('make_display')->nullable();
            $table->string('make_country')->nullable();

            $table->unique('model_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
