<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/admin', function () {
    return view('admin_list_cars');
});

Route::get('/admin/edit/{car}', function () {
    return view('admin_car_edit');
});

Route::get('/admin/create', function () {
    return view('admin_car_edit');
});
Route::get('/admin/sync','Api\SyncController@index');