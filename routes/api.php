<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {


    Route::match(['get', 'post'],'/cars/search', 'CarsController@index');
    Route::match(['put', 'patch','post'], 'cars/{car}', 'CarsController@update');
    Route::post('cars', 'CarsController@store');
    Route::delete('/cars/{car}', 'CarsController@destroy');
    Route::get('cars/{car}', 'CarsController@show');





});